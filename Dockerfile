

ARG BASE_IMAGE
# FROM almalinux/9-base
FROM ${BASE_IMAGE}


WORKDIR /

RUN git clone https://gitlab.cern.ch/YARR/YARR.git
RUN chown -R itk:itk /YARR

WORKDIR /YARR/localdb
RUN pip install --no-cache -r /YARR/localdb/setting/requirements-pip.txt

RUN dnf -y install nano

ENV HOSTNAME localdb
ENV USER itk

# RUN ./setup_db.sh

#/config/.yarr/localdb/default_host_site.json
#{
#    "institution": "default_host"
#}


# /YARR/localdb$ cat /config/.yarr/localdb/user.json 
#{
#    "userName": "itk",
#    "institution": "default_host",
#    "description": "default"
#}

COPY configs/default_host_database.json /config/.yarr/localdb/default_host_database.json

